package main

import (
      "os"
      "fmt"
      "flag"
)

func closeFile (filePointer *os.File) () {
        err := filePointer.Close()
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }
}

func main() {
        flag.Parse()
        args := flag.Args()
        if len(args) < 1 {
                fmt.Print("No file provided.")
                os.Exit(1)
        }
        filename := args[0]
        f, err := os.Open(filename)
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }
        file_stats, err := f.Stat()
        if err != nil {
                fmt.Println(err)
                closeFile(f)
                os.Exit(1)
        }
        yaml := make([]byte, file_stats.Size())
        byteCount, err := f.Read(yaml)
        if err != nil {
                fmt.Println(err)
                closeFile(f)
                os.Exit(1)
        }
        if byteCount == 0 {
                fmt.Print("Nothing read from file.")
                closeFile(f)
                os.Exit(1)
        }
        closeFile(f)
        expanded_vars := os.ExpandEnv(string(yaml))
        fmt.Println(expanded_vars)
}